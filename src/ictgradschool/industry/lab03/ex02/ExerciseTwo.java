package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {
        System.out.println( "Please enter the first integer:");
        int a = Integer.parseInt(Keyboard.readInput());
        System.out.println( "Please enter the second integer:");
        int b = Integer.parseInt(Keyboard.readInput());
        int top = Math.max(a, b);
        int bot = Math.min(a , b);
        int c = randomNumber(top, bot );
        int d = randomNumber(top, bot);
        int e = randomNumber( top, bot);
        int fin = Smallest(c, d, e);
        System.out.println( fin);

    }

    public int randomNumber(int top, int bot)
    {
        int x = (int) (Math.random()*(top - bot+1)+ bot);
        return x;

    }

    public int Smallest( int c, int d, int e)
    {
        int A = Math.min(c,d);
        int B = Math.min( d, e);
        int C = Math.min( A, B);
        return  C;

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
