package ictgradschool.industry.lab03.ex04;

import ictgradschool.Keyboard;

import static ictgradschool.Keyboard.*;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {

        String amount = getamount();
        int decimal = getdecimal();
        String modifiedNum = Cutdecimal( amount, decimal);
        prinFinalNum( decimal,modifiedNum);
    }

    // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard
    private String getamount ()
    {
        System.out.println( "Please enter amount: ");
        String amount =  Keyboard.readInput();
        return  amount;

    }
    private int getdecimal()
    {
        System.out.println( "Please enter number of decimal places that wished to be cut: ");
        int deciamalCut =  Integer.parseInt(Keyboard.readInput());
        return  deciamalCut;
    }

    private String Cutdecimal( String decimal, int dNum)
    {
        int dpoint = decimal.indexOf('.');
        String x =  decimal.substring(0, (dpoint + dNum +1));
        return  x;

    }
    private void prinFinalNum( int decimal, String modified)
    {
System.out.println( "Amount truncated to " +decimal+ " decimal places is: " +modified );
    }



    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard

    // TODO Write a method which truncates the specified number to the specified number of DP's

    // TODO Write a method which prints the truncated amount

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
